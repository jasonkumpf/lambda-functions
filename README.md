# Lambda Functions

These functions are for `DevOps` team to manage things like the triggered alerts from CloudWatch about unresponsive nodes in EKS clusters.  There is also a blank python-based lambda function to use as a starter.  More information to come.
